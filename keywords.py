#!/usr/bin/python3

import utils
import logic

FILE_NAME = 'text.txt'

TEXT_PATH = 'data/' + FILE_NAME
IGNORED_WORDS_PATH = 'data/ignored_words.txt'

IGNORED_WORDS = set(open(IGNORED_WORDS_PATH, 'r').read().split(','))
IGNORED_SYMBOLS = set([
	',', '.', ';', ':', ' - '
])

text = open(TEXT_PATH, 'r').read()

words = logic.prepare(text, IGNORED_WORDS, IGNORED_SYMBOLS)

freq = logic.calculate_frequency(words)

rank = logic.calculate_rang_by_frequency(freq)

print("freq: ")
for f in sorted(freq.items(), key=lambda kv: kv[1]):
	print(f)

print('-----------------------------------------------------------------------------------------')

print("rank: ")

# print(rank)
for r in sorted(rank.items(), key=lambda kv: kv[1]):
	print(r)
